#include <iostream>

using namespace std;

// Exemplo de função recursiva
int exec(int x) {

	// Condição de parada
	if (x < 5) {
		return (3 * x);
	} else { 
	
		// Chama a própria função
		(2 * exec(x - 5) + 7);
	}
}

int main() {
	int a, b;

	cout << "Digite um valor:";
	cin >> a;

	b = exec(a);

	cout << "Resultado: " << b << "\n";

	return 0;
	
}
