// Adicionando bibliotecas necessárias
// stdio.h é responsável pelas funções de entrada e saída de 
// dados: printf e scanf
#include <stdio.h>
// conio.h só é necessária para usuários Windows. 
// É responsável pela função getch() 
//#include <conio.h>

int main() {     
	int opcao, ent, resto[10], i, cont; 

	printf("Digite uma opcao:\n\n");
   printf("\tDecimal p/ Binario: 1 + <Enter>\n");     
	printf("\tDecimal p/ Hexadecimal: 2 + <Enter>\n");     
	printf("\tDecimal p/ Octal: 3 + <Enter>\n"); 
	scanf("%d",&opcao);

	// Este bloco converte de decimal para binário
	switch(opcao)    
	{
		case 1:            
			i = 0;             
			printf("Digite um numero decimal: ");             
			scanf("%d",&ent);             

			while(ent >= 1)             
			{
				// a variavel resto é um vetor e o % corresponde ao 
				// mod
				resto[i] = ent % 2;                       
				ent = ent / 2;

				// aqui eu incremento o contador
			   i++; 
			}
			       
			cont = i - 1;             
			while(cont >= 0)             
			{                
				printf("%d",resto[cont]);

				// aqui eu decremento o contador, para mostrar 
				// os valores do ultimo para o primeiro
			   cont--;                        
			}
		  break;

		// Este bloco converte de decimal para hexadecimal
		case 2:            
			i = 0;             
			printf("Digite um numero decimal: ");             
			scanf("%d",&ent);             

			while(ent >=15)             
			{                
				resto[i] = ent % 16;                
				ent = ent / 16;                
				i++;                       
			}             

			resto[i] = ent % 16;             
			cont = i;             

			while(cont >= 0)             
			{                
				if(resto[cont] == 10) 
					printf("A");                
				else if(resto[cont] == 11) 
					printf("B");                
				else if(resto[cont] == 12) 
					printf("C");                
				else if(resto[cont] == 13) 
					printf("D");                
				else if(resto[cont] == 14) 
					printf("E");                
				else if(resto[cont] == 15) 
					printf("F");                        
				else 
					printf("%d",resto[cont]);                
				cont--;                        
			}                              
			break;

			// Este bloco converte de decimal para octal case 3:
			case 3:
				i = 0;             
				printf("Digite um numero decimal: ");             
				scanf("%d",&ent);             

				while(ent >= 8)             
				{                
					resto[i] = ent % 8;                
					ent = ent / 8;                
					i++;                       
				}             

				resto[i] = ent % 8;             
				cont = i;             

				while(cont >= 0)             
				{                
					printf("%d",resto[cont]);                
					cont--;                        
				}                    
			break;

			// Caso o usuario digite um caracter, diferente de 1, // 2 e 3
			default:                
				printf("Opcao invalida!!!\n");      
			break;                  
		}	// Fim do switch

	printf("\n"); 

	// Esta funcao pausa a execucao e aguarda uma intervenção 
	// do usuario para sair do programa. Obs.: Só funciona no 
	// Windows
	//getch(); 

	return(0); 
}
